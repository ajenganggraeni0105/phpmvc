<?php
   class Blog extends Controller {
       public function index() {
           $data["judul"] = "Blog";
           $data["blog"] = $this->model("Blog_model")->getAllBlog();
           $this->model("Blog_model")->getAllBlog();
           $this->views("templates/header", $data);
           $this->views("blog/index", $data);
           $this->views("templates/footer");
       }
       public function detail($id) {
           $data['judul'] = "Detail Blog";
           $data['blog'] = $this->model("Blog_model")->getBlogById($id);
           $this->views("templates/header", $data);
           $this->views("blog/detail", $data);
           $this->views("templates/footer");
       }
   }
?>