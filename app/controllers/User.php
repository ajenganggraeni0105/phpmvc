<?php

class User extends Controller {
   public function index() {
      $data["judul"] = "User";
      $this->views("templates/header", $data);
      $this->views("user/index");
      $this->views("templates/footer");
   }

   public function profile($nama = "Linux", $pekerjaan = "Devs") {
      $data["judul"] = "Profile";
      $data["nama"] = $nama;
      $data["pekerjaan"] = $pekerjaan;
      $this->views("templates/header", $data);
      $this->views("user/profile", $data);
      $this->views("templates/footer");
   }
}
