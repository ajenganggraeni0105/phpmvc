<div class="container mt-3">
    <div class="row">
        <div class="col-6">
          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#formBlog">
              +
          </button>
          <br><br>
            <h3>Blog</h3>
            <ul class="list-group">
            <?php foreach($data["blog"] as $blog) :?>
                <li class="list-group-item List-group-item d-flex justify-content-between align-items-center">
                    <?= $blog['judul']; ?>
                    <a href="<?= BASE_URL;?>/blog/detail/<?= $blog['id'];?>"
                    class="badge badge-primary">baca</a>
                    <a href="" class="badge text-bg-primary">detail</a>
            </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="formBlog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>